
# Products Coding Challenge

We want to get a simple shopping cart. For this, we need to build a NodeJs application that allows us to:

* Get all available products.
* Add products to a cart.
* Delete products from a cart.
* Get cart products.
* Check at all times the available stock.
* **Extra:** It's good to know what the best products to buy are, or maybe the best sold products in the store.

## Rules of the challenge
* It should be solved on a weekend (3 days).
* The solution should take between 8hr to 10hr.
* The project needs to be published on github or bitbucket personal repository.
* The solution will be presented in front of the Development Team 1 or 2 days after the presentation.
* Tech Stack to be used is NodeJs

## What you have to code

* A NodeJs application with all endpoints to solve our exercise.
* Postman collection file. (https://www.postman.com/downloads/)

## Skills we are going to assert:

**General coding skills:**

* How readable your code is
* How you structure your code

**Modeling skills:**

* Which model you create to solve the problem

**Problem solving skills:**

* How you solve logical problems
* How you use the tools the language provides
* How well you know the best practices of the language
* Which patterns you put in practice
* Which best practices you use
* Basic git skills, how you organize the changes.


**Skills we are NOT going to assert:**

* Page load performance optimization
* Extra features we did not ask for our application

## How to start with the exercise

This projects works with Docker, so you need to have docker installed: https://www.docker.com/

To use Docker on Windows: https://docs.docker.com/docker-for-windows/install/

**If you have problems with Docker, or prefer other way its fine, we will analyze the coding and problem solving.**

To start with the exercise you need to run this command to build the Docker containers, and the API server will be lanched at http://localhost:3000/ :

```
    ./up.sh
```

If you want to load some example products you can use our seeders:

```
    ./seeders.sh
```    
